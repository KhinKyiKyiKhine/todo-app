function newTodoListElement() {
    // select/get id of text box
    const myTodoInput = document.getElementById('my-todo-data-input');

    // create list item when clicking on Add button
    // create list
    const myTodoList = document.createElement('li');

    //create select option
    let todoSelect = document.createElement('select');
    todoSelect.className = 'todo-select-option';
    let optionValueDefault = document.createElement('option');
    let optionValueComplete = document.createElement('option');
    let optionValuePending = document.createElement('option');
    let optionValueProcessing = document.createElement('option');
    optionValueDefault.text = 'select menu';
    optionValueDefault.selected= 'true';
    optionValueDefault.disabled = 'true';
    optionValueComplete.text = 'complete';
    optionValuePending.text = 'pending';
    optionValueProcessing.text = 'processing';

    todoSelect.append(optionValueDefault, optionValueComplete, optionValuePending, optionValueProcessing);

    todoSelect.onchange = function() {
        if (todoSelect.selectedIndex === 1) {
            myTodoList.style.backgroundColor = '#ede7f6';
            myTodoList.style.borderLeft = '8px solid #5e35b1';
            myTodoList.style.textDecoration = 'line-through';
            myTodoList.style.color = '#9e9e9e';
        } else if (todoSelect.selectedIndex === 2) {
            myTodoList.style.backgroundColor = '#ffecb3';
            myTodoList.style.borderLeft = '8px solid #ff7043';
            myTodoList.style.textDecoration = 'none';
            myTodoList.style.color = '#424242';
        } else if (todoSelect.selectedIndex === 3) {
            myTodoList.style.backgroundColor = '#bbdefb';
            myTodoList.style.borderLeft = '8px solid #00acc1'; 
            myTodoList.style.textDecoration = 'none';
            myTodoList.style.color = '#424242';
        }
    }

    // create text node to appear list text
    const textValue = document.createTextNode(myTodoInput.value);
    
    // create todo x(close button)
    const todoSpan = document.createElement('span');
    const txtClose = document.createTextNode('\u00D7');
    todoSpan.className = 'close-button';
    todoSpan.appendChild(txtClose);

    // append to todo list
    myTodoList.append(todoSelect, textValue, todoSpan);


    // if checking of todo input value is blank
    if (myTodoInput.value === '') {
        alert('You must write something!');
    } else {
        document.getElementById('my-todo-data').appendChild(myTodoList);

        // after created new todo list element, todo input value is set to blank
        myTodoInput.value = '';
    }

    // when click x(close text), to disappear todo list
    todoSpan.addEventListener('click', function() {
        todoSpan.parentElement.style.display = 'none';
    });
}